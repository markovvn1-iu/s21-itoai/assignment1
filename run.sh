#!/bin/bash

HOME_PATH="$(dirname "$(realpath "$0")")"

docker run --rm --name itoai-assignment1 \
	-v "${HOME_PATH}/src/:/src" \
	itoai-assignment1