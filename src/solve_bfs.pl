:- [agent].

/*
	Solve task via bfs.
	General idea:
		1. Generate all possible paths from start point and length 1 (1 in total).
		2. If number of paths is zero then return not_found.
		3. From previous paths generate all paths with length +1.
		4. If any of these paths is a solution then dispaly output, else go to 2.
*/


% is this cell home? %
bfs_step_one_agent_one_pos(ModelWorld, NextPos, OldAgent, _, [], _, [], ResultPath) :-
	AgentPath-_ = OldAgent,
	public_ModelWorld__is_home(ModelWorld, NextPos),
	ResultPath = [NextPos|AgentPath].

% is this cell anticovid and we are not protected yet? %
bfs_step_one_agent_one_pos(ModelWorld, NextPos, OldAgent, Agents, NewAgents, Used1-Used2, NewUsed, not_found) :-
	AgentPath-false = OldAgent,
	public_ModelWorld__is_anticovid(ModelWorld, NextPos),
	NewAgents = [[NextPos|AgentPath]-true|Agents],
	NewUsed = [NextPos|Used1]-[NextPos|Used2].

% are we not protected? %
bfs_step_one_agent_one_pos(_, NextPos, OldAgent, Agents, NewAgents, Used1-Used2, NewUsed, not_found) :-
	AgentPath-false = OldAgent,
	NewAgents = [[NextPos|AgentPath]-false|Agents],
	NewUsed = [NextPos|Used1]-Used2.

% are we protected? %
bfs_step_one_agent_one_pos(_, NextPos, OldAgent, Agents, NewAgents, Used1-Used2, NewUsed, not_found) :-
	AgentPath-true = OldAgent,
	NewAgents = [[NextPos|AgentPath]-true|Agents],
	NewUsed = Used1-[NextPos|Used2].



/*
	Check if we can take NextPos or not.
*/
bfs_check_step(ModelWorld, Used1-_, false, NextPos) :-
	check_step(ModelWorld, Used1, false, NextPos).
bfs_check_step(ModelWorld, _-Used2, true, NextPos) :-
	check_step(ModelWorld, Used2, true, NextPos).



/*
	Loop for all next positions of one agent.
*/
bfs_step_one_agent(ModelWorld, [NextPos|NextPoses], OldAgent, Agents, NewAgents, Used, NewUsed, not_found, ResultPath) :-
	_-IsProtected = OldAgent,
	bfs_check_step(ModelWorld, Used, IsProtected, NextPos),
	bfs_step_one_agent_one_pos(ModelWorld, NextPos, OldAgent, Agents, TempAgents, Used, TempUsed, TempResultPath),
	bfs_step_one_agent(ModelWorld, NextPoses, OldAgent, TempAgents, NewAgents, TempUsed, NewUsed, TempResultPath, ResultPath).

% Skip NextPos %
bfs_step_one_agent(ModelWorld, [_|NextPoses], OldAgent, Agents, NewAgents, Used, NewUsed, not_found, ResultPath) :-
	bfs_step_one_agent(ModelWorld, NextPoses, OldAgent, Agents, NewAgents, Used, NewUsed, not_found, ResultPath).

% Path were found or NextPoses is empty %
bfs_step_one_agent(_, _, _, Agents, Agents, Used, Used, ResultPath, ResultPath).



/*
	Loop for all Agents.
*/
bfs_step(ModelWorld, [OldAgent|OldAgents], Agents, NewAgents, Used, NewUsed, not_found, ResultPath) :-
	[AgentPos|_]-_ = OldAgent,
	agent_step_list(AgentPos, NextPoses),
	bfs_step_one_agent(ModelWorld, NextPoses, OldAgent, Agents, TempAgents, Used, TempUsed, not_found, TempResultPath),
	bfs_step(ModelWorld, OldAgents, TempAgents, NewAgents, TempUsed, NewUsed, TempResultPath, ResultPath).

bfs_step(_, _, Agents, Agents, Used, Used, ResultPath, ResultPath).



% no agents, stop solving %
solve_bfs(_, [], _, not_found, not_found) :- !, fail.

% backtrack worker loop %
solve_bfs(ModelWorld, OldAgents, Used, not_found, ResultPath) :-
	bfs_step(ModelWorld, OldAgents, [], NewAgents, Used, NewUsed, not_found, TempResultPath), !,
	solve_bfs(ModelWorld, NewAgents, NewUsed, TempResultPath, ResultPath).

solve_bfs(_, _, _, ResultPath, ResultPath).

% bfs entrypoint %
solve_bfs(ModelWorld, ResultPath) :-
	public_ModelWorld__is_start(ModelWorld, StartPos), !,
	solve_bfs(ModelWorld, [[StartPos]-false], [StartPos]-[], not_found, TempResultPath),
	reverse(TempResultPath, ResultPath).