:- [agent].

/*
	Solve task via backtracking.
	General idea:
		1. Find ANY solution.
		2. Find solution shorter than previous solution.
		3. If such solution exist then repeat point 2 else go to point 4.
		4. Previous path is the shortest one. Display output.
*/



/*
	Find a path that is shorter than MaxPathLen.
*/
% is this cell home? %
find_shorter_path(ModelWorld, [AgentPos|Path], _, _, [AgentPos|Path]) :-
	public_ModelWorld__is_home(ModelWorld, AgentPos), !.

% is this cell anticovid and we are not protected yet? %
find_shorter_path(ModelWorld, [AgentPos|Path], false, MaxPathLen, ResultPath) :-
	public_ModelWorld__is_anticovid(ModelWorld, AgentPos),
	find_shorter_path(ModelWorld, [AgentPos|Path], true, MaxPathLen, ResultPath).

% is this cell just usual cell? %
find_shorter_path(ModelWorld, [AgentPos|Path], IsProtected, MaxPathLen, ResultPath) :-
	length([AgentPos|Path], PathLen),
	PathLen + 1 < MaxPathLen,

	agent_step(AgentPos, NextPos),
	check_step(ModelWorld, Path, IsProtected, NextPos),
	find_shorter_path(ModelWorld, [NextPos,AgentPos|Path], IsProtected, MaxPathLen, ResultPath).



% backtrack entrypoint %
solve_backtracking(ModelWorld, ResultPath) :-
	solve_backtracking(ModelWorld, not_found, 1000, ResultPath).

% backtrack worker loop %
solve_backtracking(ModelWorld, _, MaxPathLen, ResultPath) :-
	public_ModelWorld__is_start(ModelWorld, StartPos),
	find_shorter_path(ModelWorld, [StartPos], false, MaxPathLen, NewBestPath),
	length(NewBestPath, NewBestPathLen),
	format("[INFO] Find solution with ~d steps~n", [NewBestPathLen]),
	solve_backtracking(ModelWorld, NewBestPath, NewBestPathLen, ResultPath).

% no solutions were found %
solve_backtracking(_, not_found, _, _) :- !, fail.

% backtracking successfully complete %
solve_backtracking(_, BestPath, _, BestPath) :-
	format("[INFO] Backtracking complete~n"), !.