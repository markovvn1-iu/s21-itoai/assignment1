check_uniqueness(X) :-
	sort(X, Xt),
	length(X, XLen),
	length(Xt, XtLen),
	XLen = XtLen.

random_pos(Y/X, [H, W]) :-
	random(0, H, Y),
	random(0, W, X).


%================================ ModelWorld ================================%

/*
class ModelWorld
{
private:
	// check list of poses
	void check_pos_list(list);

	// check if any of positions is infected
	void check_is_not_infected(list);

	// check that model is correct
	void check_model();

public:
	// Create world from parameters
	void init(home_pos, covid_list, anticovid_list, field_size);

	// Generate random field
	void generate_random(field_size);

	// Check if the cell with position (y, x) exist
	void check_pos(y, x);

	// Is start cell
	void is_start();

	// Is home cell
	void is_home(y, x);

	// Is covid cell
	void is_covid(y, x);

	// Is anticovid cell
	void is_anticovid(y, x);

	// Is the cell (y, x) infected
	void is_infected(y, x);
};

To store class we use ModelWorld structure:
[StartPos, HomePos]-[Covids, AntiCovids]-FieldSize = ModelWorld

StartPos = x/y
HomePos = x/y
Covids = [x/y, x/y]
AntiCovids = [x/y, x/y]
FieldSize = [h, w]
*/

%============================================================================%

%% ModelWorld::check_pos_list %%
private_Model_world__check_pos_list(_, []).
private_Model_world__check_pos_list(ModelWorld, [Item|Poses]) :-
    public_ModelWorld__check_pos(ModelWorld, Item),
    private_Model_world__check_pos_list(ModelWorld, Poses), !.

%% ModelWorld::check_is_not_infected %%
private_ModelWorld__check_is_not_infected(_, []).
private_ModelWorld__check_is_not_infected(ModelWorld, [Pos|Poses]) :-
	\+ public_ModelWorld__is_infected(ModelWorld, Pos),
	private_ModelWorld__check_is_not_infected(ModelWorld, Poses).

%% ModelWorld::check_model %%
private_ModelWorld__check_model(ModelWorld) :- 
    [StartPos, HomePos]-[Covids, AntiCovids]-_ = ModelWorld,
    public_ModelWorld__check_pos(ModelWorld, StartPos),
    public_ModelWorld__check_pos(ModelWorld, HomePos),
    private_Model_world__check_pos_list(ModelWorld, Covids),
    private_Model_world__check_pos_list(ModelWorld, AntiCovids),

    StartPos \= HomePos,
    \+ member(StartPos, AntiCovids),
    \+ member(HomePos, AntiCovids),
    private_ModelWorld__check_is_not_infected(ModelWorld, [StartPos,HomePos|AntiCovids]),

    check_uniqueness(Covids),
    check_uniqueness(AntiCovids).

%% ModelWorld::init %%
public_ModelWorld__init(ModelWorld, StartPos, HomePos, Covids, AntiCovids, FieldSize) :-
	ModelWorld = [StartPos, HomePos]-[Covids, AntiCovids]-FieldSize,
	private_ModelWorld__check_model(ModelWorld).

%% ModelWorld::generate_random %%
public_ModelWorld__generate_random(_, [H, W]) :-
	H * W >= 10, fail;
	H * W < 7, !, fail;
	H = 3, W < 4, !, fail;
	W = 3, H < 4, !, fail.
public_ModelWorld__generate_random(ModelWorld, FieldSize) :-
	random_pos(StartPos, FieldSize),
	random_pos(HomePos, FieldSize),
	random_pos(Covid1, FieldSize),
	random_pos(Covid2, FieldSize),
	random_pos(AntiCovid1, FieldSize),
	random_pos(AntiCovid2, FieldSize),
	public_ModelWorld__init(ModelWorld, StartPos, HomePos, [Covid1, Covid2], [AntiCovid1, AntiCovid2], FieldSize), !.
public_ModelWorld__generate_random(ModelWorld, FieldSize) :-
	public_ModelWorld__generate_random(ModelWorld, FieldSize).

%% ModelWorld::check_pos %%
public_ModelWorld__check_pos(ModelWorld, Y/X) :-
	_-_-[H, W] = ModelWorld,
	Y >= 0, X >= 0, Y < H, X < W.

%% ModelWorld::is_start %%
public_ModelWorld__is_start(ModelWorld, Y/X) :-
	[Y/X, _]-_-_ = ModelWorld.

%% ModelWorld::is_home %%
public_ModelWorld__is_home(ModelWorld, Y/X) :-
	[_, Y/X]-_-_ = ModelWorld.

%% ModelWorld::is_covid %%
public_ModelWorld__is_covid(ModelWorld, Y/X) :-
	_-[Covids, _]-_ = ModelWorld,
	member(Y/X, Covids).

%% ModelWorld::is_anticovid %%
public_ModelWorld__is_anticovid(ModelWorld, Y/X) :-
	_-[_, AntiCovids]-_ = ModelWorld,
	member(Y/X, AntiCovids).

%% ModelWorld::is_infected %%
public_ModelWorld__is_infected(ModelWorld, Y/X) :-
	Y1 is Y-1, Y2 is Y+1,
	X1 is X-1, X2 is X+1,
	member(Xs, [X1, X, X2]),
	member(Ys, [Y1, Y, Y2]),
	public_ModelWorld__is_covid(ModelWorld, Ys/Xs).