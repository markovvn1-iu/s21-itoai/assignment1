:- [model].  %% Load class ModelWorld %%
:- [model_print].  %% Load class ModelWorldPrinter %%
:- [solve_backtracking].  %% Load solve_backtracking %%
:- [solve_bfs].  %% Load solve_bfs %%


% Parameters you can change %
define_solve_method(bfs).  % method which will be used to solve the problem: bfs, backtracking %
define_field_size(9, 9).  % size of the world %


% Available solve methods %
solve_method(bfs).
solve_method(backtracking).



generate_world(ModelWorld) :-
	%% public_ModelWorld__init(ModelWorld, 5/4, 0/0, [3/0, 1/3], [0/1, 1/0], [6, 6]), !.
	%% public_ModelWorld__init(ModelWorld, 8/0, 1/1, [1/6, 4/1], [4/4, 7/7], [9, 9]), !.
	define_field_size(H, W), public_ModelWorld__generate_random(ModelWorld, [H, W]), !.
generate_world(_) :-
	write("[FATAL] Error while generating the world"), nl, !, fail.



solver(ModelWorld, ResultPath, backtracking) :-
	solve_backtracking(ModelWorld, ResultPath), !.
solver(ModelWorld, ResultPath, bfs) :-
	solve_bfs(ModelWorld, ResultPath), !.
solver(_, not_found, Method) :-
	solve_method(Method), !.
solver(_, not_found, Method) :-
	write("[FATAL] Unknown method "), write(Method),
	write(". Available methods: bfs, backtracking"), nl, !, fail.



print_array([H1,H2|T]) :-
	write(H1), write(", "),
	print_array([H2|T]).
print_array([H]) :-
	write(H).


print_result_path(not_found) :-
	write("Path do not exist").
print_result_path(ResultPath) :-
	length(ResultPath, ResultPathLen),
	write("Result path (length: "), write(ResultPathLen), write("): "), print_array(ResultPath).


main() :-
	write("Generating map..."), nl,
	generate_world(ModelWorld),
	public_ModelWorldPrinter__print_world(ModelWorld),

	write("Solving..."), nl,
	define_solve_method(Method),

	get_time(TimeStamp1),
	solver(ModelWorld, ResultPath, Method),
	get_time(TimeStamp2),

	TimeStampDiff is TimeStamp2 - TimeStamp1,
	format("Solved in ~f sec~n", [TimeStampDiff]),

	print_result_path(ResultPath), nl,
	public_ModelWorldPrinter__print_world(ModelWorld, ResultPath).
main() :-
	write("[FATAL] main() failed"), nl.


?-main.