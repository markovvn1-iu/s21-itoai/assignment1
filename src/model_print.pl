:- [model].  %% Load class ModelWorld %%

%============================= ModelWorldPrinter =============================%

/*
class ModelWorldPrinter
{
private:
	// print one cell on position xy of the world
	void print_world_cell(modelWorld, xy, path);

	// print the entire line y of the world 
	void print_world_line(modelWorld, y, xs, path);

	// print the entire grid of the world
	void print_world_grid(modelWorld, ys, xs, path);

	// print horizontal line (part of the border)
	void print_horizontal_line(xs);

public:
	// print the entire world with given path
	void print_world(modelWorld, path);
};
*/

%=============================================================================%

%% ModelWorldPrinter::print_world_cell %%
private_ModelWorldPrinter__print_world_cell(ModelWorld, Y/X, Path) :-
	member(Y/X, Path), write("#");
	public_ModelWorld__is_covid(ModelWorld, Y/X), write("С");
	public_ModelWorld__is_anticovid(ModelWorld, Y/X), write("+");
	public_ModelWorld__is_home(ModelWorld, Y/X), write("F");
	public_ModelWorld__is_start(ModelWorld, Y/X), write("S");
	public_ModelWorld__is_infected(ModelWorld, Y/X), write("·");
	write(" ").

%% ModelWorldPrinter::print_world_line %%
private_ModelWorldPrinter__print_world_line(_, _, [], _).
private_ModelWorldPrinter__print_world_line(ModelWorld, Y, [X|Xs], Path) :-
	[_|_] = Xs,
	private_ModelWorldPrinter__print_world_cell(ModelWorld, Y/X, Path), write(" "),
	private_ModelWorldPrinter__print_world_line(ModelWorld, Y, Xs, Path), !.
private_ModelWorldPrinter__print_world_line(ModelWorld, Y, [X|_], Path) :-
	private_ModelWorldPrinter__print_world_cell(ModelWorld, Y/X, Path).

%% ModelWorldPrinter::print_world_grid %%
private_ModelWorldPrinter__print_world_grid(_, [], _, _).
private_ModelWorldPrinter__print_world_grid(ModelWorld, [Y|Ys], Xs, Path) :-
	write("│ "), private_ModelWorldPrinter__print_world_line(ModelWorld, Y, Xs, Path), write(" │"), nl,
	private_ModelWorldPrinter__print_world_grid(ModelWorld, Ys, Xs, Path).

%% ModelWorldPrinter::print_horizontal_line %%
private_ModelWorldPrinter__print_horizontal_line([]).
private_ModelWorldPrinter__print_horizontal_line([_|Xs]) :-
	write("──"),
	private_ModelWorldPrinter__print_horizontal_line(Xs).

%% ModelWorldPrinter::print_world %%
public_ModelWorldPrinter__print_world(ModelWorld, Path) :-
	_-_-[H, W] = ModelWorld,
	Ymax is H - 1, Xmax is W - 1,
	numlist(0, Ymax, Ys),
	numlist(0, Xmax, Xs),
	write("╭─"), private_ModelWorldPrinter__print_horizontal_line(Xs), write("╮"), nl,
	private_ModelWorldPrinter__print_world_grid(ModelWorld, Ys, Xs, Path),
	write("╰─"), private_ModelWorldPrinter__print_horizontal_line(Xs), write("╯"), nl.
public_ModelWorldPrinter__print_world(ModelWorld) :-
	public_ModelWorldPrinter__print_world(ModelWorld, []).