/*
	Generate all possible steps of agent
*/
agent_step_list(AgentPos, NextPoses) :-
	Y/X = AgentPos,
	Y1 is Y-1, Y2 is Y+1,
	X1 is X-1, X2 is X+1,
	NextPoses = [Y2/X1, Y2/X2, Y1/X1, Y1/X2, Y2/X, Y1/X, Y/X1, Y/X2].

agent_step(AgentPos, NextPos) :-
	agent_step_list(AgentPos, NextPoses),
	member(NextPos, NextPoses).

/*
	Check that the step matches the rules.
*/
check_step(ModelWorld, ExcludePos, true, NextPos) :-
	public_ModelWorld__check_pos(ModelWorld, NextPos),
	\+ member(NextPos, ExcludePos).

check_step(ModelWorld, ExcludePos, false, NextPos) :-
	public_ModelWorld__check_pos(ModelWorld, NextPos),
	\+ member(NextPos, ExcludePos),
	\+ public_ModelWorld__is_infected(ModelWorld, NextPos).