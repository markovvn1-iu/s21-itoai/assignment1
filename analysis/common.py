from collections import namedtuple
import pickle
import zlib


ReportItem = namedtuple("ReportItem", ("status", "time", "length", "path", "map"))


def parse_report(data):
	if data.startswith("timeout"):
		return ReportItem("timeout", 0, 0, [], None)

	assert data.startswith("Generating map...\n")
	data = data[len("Generating map...\n"):]

	# map
	i = data.find("\nSolving...\n")
	assert i > 0
	data_map = data[:i]
	data = data[i + len("\nSolving...\n"):]

	# time
	i = data.find("Solved in ")
	assert i >= 0
	data = data[i + len("Solved in "):]
	i = data.find(" sec\n")
	assert i > 0
	data_time = float(data[:i])
	data = data[i + len(" sec\n"):]

	if data.startswith("Path do not exist"):
		return ReportItem("not_found", data_time, 0, [], data_map)

	# length
	assert data.startswith("Result path (length: ")
	data = data[len("Result path (length: "):]
	i = data.find("): ")
	assert i > 0
	data_length = int(data[:i])
	data = data[i + len("): "):]

	# path
	i = data.find("\n")
	assert i > 0
	data_path = data[:i].split(", ")

	return ReportItem("ok", data_time, data_length, data_path, data_map)


def load_reports(file_name):
	with open(file_name, "rb") as f:
		data = pickle.loads(zlib.decompress(f.read()))

	return [parse_report(item) for item in data]