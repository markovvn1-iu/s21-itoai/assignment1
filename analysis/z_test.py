import math
import numpy as np
from common import load_reports


def calc_z_value(sample):
	# H0: the sample has mu=0
	assert len(sample) > 30, "use t-test instead"

	mean = sample.mean()
	std = ((sample ** 2).mean() - mean ** 2) ** 0.5

	return mean * len(sample) ** 0.5 / std

def p_value_from_z_value(z_value):
	return 1 - math.erf(z_value / math.sqrt(2))



reports1 = load_reports("var1/bfs.log")
reports2 = load_reports("var2/bfs.log")
# reports1 = load_reports("var1/backtracking.log")
# reports2 = load_reports("var2/backtracking.log")

reports1 = [i.time for i in reports1 if i.status == "ok"]
reports2 = [i.time for i in reports2 if i.status == "ok"]
np.random.shuffle(reports1)
np.random.shuffle(reports2)

N = min(len(reports1), len(reports2))
reports1, reports2 = reports1[:N], reports2[:N]

print("number of samples: ", N)
z_value = calc_z_value(np.array(reports1) - np.array(reports2))
print("z-value: ", z_value)
print("p-value: ", p_value_from_z_value(z_value))