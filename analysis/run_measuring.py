from collections import namedtuple
from tqdm import trange
import subprocess
import zlib
import pickle


def measure(timeout):
	try:
		res = subprocess.run(["../run.sh"], capture_output=True, timeout=timeout)
		return res.stdout.decode()
	except subprocess.TimeoutExpired:
		subprocess.run(["./kill.sh"], capture_output=True)
		return "timeout"


def test_bfs(file_name):
	res = []

	try:
		for i in trange(100):
			res.append(measure(timeout=5))
	finally:
		with open(file_name, "wb") as f:
			f.write(zlib.compress(pickle.dumps(res)))


def test_backtracking(file_name):
	res = []

	try:
		for i in trange(10000):
			res.append(measure(timeout=2500))
	finally:
		with open(file_name, "wb") as f:
			f.write(zlib.compress(pickle.dumps(res)))

# test_backtracking("backtracking.log")
test_bfs("bfs.log")