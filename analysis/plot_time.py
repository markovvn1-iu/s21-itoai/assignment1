from collections import namedtuple, defaultdict
from .common import load_reports
import math
import matplotlib.pyplot as plt



def plot_time_lin(axis, reports, plot_points=10):
	reports_time = [i.time for i in reports if i.status == "ok"]
	min_time, max_time = min(reports_time), max(reports_time)
	point_sz = max_time - min_time

	res = [0] * plot_points
	res_time = [point_sz * ((i+1) / (2 * plot_points)) for i in range(plot_points)]

	for i in reports_time:
		group_id = int((i - min_time) / (max_time - min_time) * plot_points)
		group_id = min(max(0, group_id), plot_points-1)
		res[group_id] += 1

	assert sum(res) == len(reports_time)
	# res = [i * plot_points for i in res]
	# res = [res[i] / point_sz for i in range(plot_points)]
	# k = sum((x1 + x0) * (t1 - t0) / 2 for x0, x1, t0, t1 in zip(res, res[1:], reports_time, reports_time[1:]))
	# print(k)
	# res = [i / k for i in res]
	
	axis.set_xlabel("time")
	axis.set_ylabel("# of samples")
	axis.plot(res_time, res)


def plot_time_log(axis, reports, plot_points=10):
	reports_time = [i.time for i in reports if i.status == "ok"]
	min_time, max_time = min(reports_time), max(reports_time)
	point_sz = (max_time / min_time) ** (1/plot_points)

	res = [0] * plot_points
	res_time = [min_time * (point_sz ** i + point_sz ** (i+1))/2 for i in range(plot_points)]

	for i in reports_time:
		group_id = int(math.log(i / min_time, point_sz))
		group_id = min(max(0, group_id), plot_points-1)
		res[group_id] += 1

	assert sum(res) == len(reports_time)
	# res = [i / len(reports_time) for i in res]

	axis.set_xlabel("time")
	axis.set_ylabel("# of samples")
	axis.set_xscale("log")
	axis.plot(res_time, res)


def compute_length(reports):
	reports = [i for i in reports if i.status == "ok"]
	# res = [[] for i in range(max([i.length for i in reports]) + 1)]
	res = defaultdict(list)

	for i in reports:
		res[i.length].append(i.time)

	res = sorted([(k, sum(v) / len(v)) for k, v in res.items()])
	return [i[0] for i in res], [i[1] for i in res]


def plot_length_log(axis, reports):
	axis.set_xlabel("path length")
	axis.set_ylabel("time")
	axis.set_yscale("log")
	axis.plot(*compute_length(reports))

def plot_length_lin(axis, reports):
	axis.set_xlabel("path length")
	axis.set_ylabel("time")
	axis.plot(*compute_length(reports))


def compute_status_statistic(reports):
	res = defaultdict(lambda: 0)
	for i in reports:
		res[i.status] += 1
	return dict(res)

def compute_mean_var_time(reports):
	reports_time = [i.time for i in reports if i.status == "ok"]

	mean = sum(reports_time) / len(reports_time)
	variance = sum(i ** 2 for i in reports_time) / len(reports_time) - mean ** 2
	return mean, variance


bfs_reports = load_reports("var2/bfs.log")
backtracking_reports = load_reports("var2/backtracking.log")

print("bfs: ", compute_status_statistic(bfs_reports))
print("backtracking: ", compute_status_statistic(backtracking_reports))

mean, variance = compute_mean_var_time(bfs_reports)
print(f"bfs: mean = {mean}, std = {variance ** 0.5}")

mean, variance = compute_mean_var_time(backtracking_reports)
print(f"backtracking: mean = {mean}, std = {variance ** 0.5}")

fg, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2, figsize=(16, 8))
plot_time_log(ax1, backtracking_reports, plot_points=20)
plot_length_lin(ax2, backtracking_reports)
plot_time_lin(ax3, bfs_reports, plot_points=20)
plot_length_lin(ax4, bfs_reports)
plt.show()