# Assignment 1

Vladimir Markov B19-03 (v.markov@innopolis.university)

### Program structure

- **agent.pl** - functions related to agent: get a list of steps, check if next step is allowed
- **model.pl** - functions related to world model: get start position, get end position, get position of mask, generate random field of given size
- **model_print.pl** - functions for print world model
- **solve_backtracking.pl** - functions for solving given problem with the use of backtracking in prolog
- **solve_bfs.pl** - functions for solving given problem with the use of the bfs-like method
- **main.pl** - main module. Runnable

### Run program

Build docker image with swi-prolog:

```bash
./build.sh
```

Run docker container:

```bash
./run.sh
```

### Change parameters

All parameters that you can change are in `main.pl` file under the comment `% Parameters you can change %`. You can change solve method that used to solve (`define_solve_method`) and size of field (`define_field_size`)

Valid methods: bfs, backtracking

### Assignment report

See [assignment1_report.md](assignment1_report.md)