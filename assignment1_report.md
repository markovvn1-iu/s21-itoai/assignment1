# Assignment 1. Report

## Algorithm flow

### Backtracking

Backtracking in prolog is the way to solve problems. For this, you need to specify all constraints on the solution and then prolog finds all solutions by itself. Prolog doing it by enumerate all possible combinations of steps and finding which one is pass all constrains. But if path is longer than 8, then it might take to much time. To speed up this process let's use following algorithm.

First, program find any solution (length is not important). Then program try to find shorter solution than previous one. If such a solution exists then repeat previous step and try to find shorter solution than this one. If there is no solution, then previous one is the shortest one. We can display output.

To implement this we need to create two functions. The first function will looking for solution that shorter than given one. The second function will menage all other logics of algorithm. It will decide whether to continue searching for solution of to we already may show the result.

### BFS

Implementing BFS algorithm in prolog is much harder than backtracking. The first problem we may face is what to do with the mask and the doctor: sometimes we have to go back to pick up the mask/the doctor. To handle them properly I decided to consider the field like two-storey building, where the first floor contains walls (covids) and two one-direction stairs from the first to the second floor (the doctor and the mask). The home is placed on both floors.

BFS starts from the start point on the first floor and spreads evenly in all directions (including the second floor through the doctor or the mask). To not visited two place twice it use array `used` where it store places where it already was.

## Statistical comparison

To create a quality statistical analysis we need to run program for thousand times. I have run each program (bfs-var1, bfs-var2, backtracking-var1, backtracking-var2) for 10000 times on randomly generated maps (it took about 29 hours). After that I have plot 2 plots for each program. The first plot show distribution of time to find the path. The second plot show dependency between computation time and length of path to home

**Backtracking (variant 1)**
![backtracking_var1](.gitlab/backtracking_var1.png)

**Backtracking (variant 2)**
![backtracking_var2](.gitlab/backtracking_var2.png)

**BFS (variant 1)**
![bfs_var1](.gitlab/bfs_var1.png)

**BFS (variant 2)**
![bfs_var2](.gitlab/bfs_var2.png)

A few comments on these plots:

- Variant 1 and variant 2 are almost the same.

- The solution time depends exponentially on the path length for backtracking. And almost linear for BFS.

- The sharp drop down after 12 on the second plot for backtracking is due to the fact that if the solution was not found in a few seconds, the program is likely to exceed the timeout of 2500 seconds (plot do not contains solutions that exceed the timeout).

### Mean, standard deviation and timeouts

| Algorithm                | Percentage of timeouts | Time mean (ms) | Time std (ms) |
| ------------------------ | ---------------------- | -------------- | ------------- |
| Backtracking (variant 1) | 1.24%                  | 1442.4         | 7721.6        |
| Backtracking (variant 2) | 1.31%                  | 1364.1         | 7406.2        |
| BFS (variant 1)          | 0%                     | 0.6951         | 0.5871        |
| BFS (variant 2)          | 0%                     | 0.6894         | 0.5845        |

As you can see, BFS is much faster and much stable compare to backtracking and also variant 1 and variant 2 are almost the same.

### Play with z-test (backtracking)

We have a lot of data, so, let's use z-test (t-test works well for small sample size).

**Simple sample**: time measure of Backtracking (var 1) minus time measure of Backtracking (var 2).

**H0**: Our simple sample has normal distribution with μ = 0.

**H1**: μ is not equal 0

Let's calculate z-value for our simple samples: 0.73197. And now we can calculate p-value (the probability that an observed difference could have occurred just by random chance): 46.42%. So, the data do not contradict the null hypothesis. It means that either Backtracking (var 1) **has the same distribution** as Backtracking (var 2) or **our sample is not enough large** to observe significant difference.

*Notice: if you want to do to dark deep details of how to compute this numbers, we may take a look at the `analysis/z_test.py` program that I used to do this.*

### Play with z-test (bfs)

Actually, just run program `analysis/z_test.py` once again but with another simple sample. So, p-value is 49.49%. It means that either BFS (var 1) **has the same distribution** as BFS (var 2) or **our sample is not enough large** to observe significant difference.

### Direct comparison (only for assignment)

Backtracking (variant 1) compared to 2nd algorithm (variant 1). Second is faster

Backtracking (variant 2) compared to 2nd algorithm (variant 2). Second is faster

Backtracking (variant 1) compared to Backtracking (variant 2).  Looks like they are the same

2nd algorithm (variant 1) compared to 2nd algorithm (variant 2).  Looks like they are the same

## PEAS model

(no information that it should be written in form of essay)

- **Performance measure**
  - The agent reach home (in the shortest path)

- **Environment**:
  - Field 9x9 (or other size) with two COVIDs, the mask, the doctor and the home.
  - Properties of the Environment
    - Partially observable (agent do not see the whole field)
    - Single agent (there is only one agent)
    - Deterministic (we know our next position before we apply action)
    - Sequential (previous actions effect on next action; we may use information about places where we already was and, for example, do not visit them again)
    - Static (environment do not change as the agent is thinking about its actions)
    - Discrete (There are states in the environment)
    - Known (Designer of the agent have full knowledge of the rules of the environment)

- **Actuators**:
  - move one cell in any direction (if possible)
  - pick mask/doctor (if exist in current cell) and protect itself from COVID
- **Sensors**:
  - perceive the current cell (is it free or is there a doctor/mask or is it infected)
  - sees position of the home (knows exact coordinates of the home)
  - sees his position (knows exact coordinates of itself)
  - perceive infected cell (range depends on the chosen variant - 1 or 2)

## Maps that were impossible to solve

```
╭───────────────────╮  ╭───────────────────╮
│   + · · ·         │  │     · · ·         │
│ + F · С ·         │  │   S · С ·         │
│     · · ·         │  │     · · ·         │
│ · · ·             │  │ · · ·             │
│ · С ·             │  │ · С ·   +         │
│ · · ·             │  │ · · ·   +         │
│                   │  │                   │  + - mask or doctor
│                   │  │                   │  C - COVID
│ S                 │  │ F                 │  S - start
╰───────────────────╯  ╰───────────────────╯  F - finish
```

These maps impossible to solve because there is no path from start to finish (home).

